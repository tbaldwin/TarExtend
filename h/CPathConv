/* Copyright 1999 Element 14 Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/************************************************************************/
/* File:    CPathConv.h                                                 */
/* Purpose: Stores 2 Risc OS filepaths.                                 */
/*                                                                      */
/* Author:  David Cotton <mailto:dcotton@e-14.com>                      */
/* History: 0.01  1999-01-28                                            */
/*                Created.                                              */
/************************************************************************/

// Include Application headers
#include "types.h"

/* ----------------------------------------- FUNCTIONS ------------------------------------------ */
class CPathConv
{
  public:
    CPathConv(char* original_path, char* replacement_path);
    CPathConv(const char* original_path, const char* replacement_path);
    CPathConv(CPathConv& path);
    CPathConv(void);
    ~CPathConv();

    void display(void);
    char* get_original_value(void);
    char* get_replacement_value(void);
    BOOL set_data(char* new_original_path, char* new_replacement_path);

    friend ostream& operator <<(ostream& stream, CPathConv& path);
    friend int operator <(CPathConv path, CPathConv path2 );
    friend int operator >(CPathConv path, CPathConv path2 );
    friend int operator ==(CPathConv path, CPathConv path2 );
    int operator =(CPathConv path);

  private:
    BOOL compare(CPathConv* path);
    BOOL compare(CPathConv path);
    char* original_path;
    char* replacement_path;
};
